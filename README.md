<h5>Startseite</h5>
<div>https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/main/html_slides/startpage.html </div>
<br>


<h5>Erklärungspräsentation</h5>
<div>https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/main/html_slides/erklärungspräsentation.html </div>
<br>

<h5>Dokumentation</h5>
<div> RevealJS: https://revealjs.com/ </div>
<div> Markdown: https://www.markdownguide.org/basic-syntax/ </div>
<div> Schnittstelle: https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/main/html_slides/dokumentation.html </div>
<br>

<h5> Links zu den Schulungsmaterialien des Beispielrepositories </h5>

<br>

<div>Demo: https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/revealjs/html_slides/demo.html</div>
<div>Testumgebung:  https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/revealjs/html_slides/testumgebung.html </div>
<div>Beispielvorlage: https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/revealjs/html_slides/beispielvorlage.html </div>
<div>Download unter: https://git.rwth-aachen.de/nfdi4ing/education/education-pages/revealjs/-/jobs/artifacts/master/raw/public/Kurs.pdf?job=pages </div>

<br>

<h5> Links zu Ihrem individuellen Schulungsmaterialien </h5>

<br>

<div>Testumgebung:  https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/testumgebung.html </div>

<div>Ihre Präsentation: https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/nfdi4ing-archetypen/html_slides/main.html </div>
<div>Download unter: https://git.rwth-aachen.de/nfdi4ing/education/education-pages/nfdi4ing-archetypen/-/jobs/artifacts/master/raw/public/Kurs.pdf?job=pages </div>

<br>

### Reuse
If you wish to re-use these documents, please select refer to our license.

To repurpose materials from this GitLab, we recommend the following for full attribution:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> This content was re-used from the [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) under a  <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License (CC BY 4.0)</a>. The re-used materials can be found under: https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main

If it is not possible to list the full names, please use the following link:
> https://git.rwth-aachen.de/nfdi4ing/education/education-pages/main/-/blob/master/LICENSE

Further information on licences and the re-use of the materials provided here can be found [here].(https://git.rwth-aachen.de/groups/nfdi4ing/education/-/wikis/Lizenzen) (german).

### Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> This site and its contents are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Attribution 4.0 International License (CC BY 4.0)</a>.

